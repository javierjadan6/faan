<?php

/*
Plugin Name: Members
Descripction: Muestra informacion de bienvenida a tu usuarios 
Author: Javier Jadan
Version: 1.0.0

*/

// Create the shortcode

add_shortcode('show_user_info', 'ui_show_user_info');


function ui_show_user_info(){


    $userID = get_current_user_id();

    if($userID != 0):
        $userinfo= get_userdata($userID);
        $userName = $userinfo ->first_name;
        $userbarrio = $userinfo-> show_admin_bar_front;
        if(empty($userName)):
            $userName=$userName-> user_login;
        endif;
    
        ob_start();
        ?>
        <p class="user_info">
            <?php echo $userName ; ?>  
            Gracias por resgistrarte, tu barrio es
            <?php echo $userbarrio ; ?>
            de cuenca    

         </p>
            
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    
    else: 
        return;    
    
    endif;

}



 
 