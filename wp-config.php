<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'faan' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3Q[EmZiV F[N.5]1!i|WMaH~zAs7R5Qb#^0FWyEW4I[dUS2p<eTJa#_JA)r&Si.r' );
define( 'SECURE_AUTH_KEY',  '1X|);yM@h$^t1KQrtEybk&tZObV6XJeNHUw3CW>H2wIzyHgSIfjx46MjJYGZ2nQT' );
define( 'LOGGED_IN_KEY',    'z2*|+:+TS5VQ)X)B6WLX/xugcH (,TmGo}#$n5D`w%(RX(?:~TSIYFD?|f[{~;Nd' );
define( 'NONCE_KEY',        '4;[ !Ko9:N,$Q$o#CfUv49>DgjOCrmifWO*1Ml8%R#@cir:<]d4[`cYxc_6l, Y0' );
define( 'AUTH_SALT',        'VYV,>+}QQB)BUF3zcw1hKFn~p![*:B]BIKru@rc/B.<q#sm`isj/8U>Cfb{qRhPT' );
define( 'SECURE_AUTH_SALT', '0{1PBt/&?$DSM=)I71.PiK0BVOO/Oj>B)+Ts_BT*h89jU:H9 :ppFvslp_wrb6C;' );
define( 'LOGGED_IN_SALT',   '_Yef,0Cs[]9NMI]7(.P<P@%00j~l1TrGMV:C]>Yb;oq?-wexy+Q~o9=zj>9d45Em' );
define( 'NONCE_SALT',       'lRNRv<MUqkRqYUCT<2Y$~<QbbW.&@fvvWqH.`9*A,qwNFk;jeUF!,%^&t5n/L@.Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
